﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicsEditor
{
    public partial class Form1 : Form
    {
        Color CurrentColor = Color.Black; //Default color
        Point CurrentPoint; //Current Position
        Point PrevPoint; //Previous Position
        bool isPressed = false;
        Graphics g;
        Point firstp = new Point();
        Line line = null;
        List<Line> lines = new List<Line>();
        
        public event MouseEventHandler upEvents;
        public event MouseEventHandler downEvents;
        public event MouseEventHandler moveEvents;

        public Form1()
        {
            InitializeComponent();     
            g = Graphics.FromHwnd(pictureBox1.Handle);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var fileName = openFileDialog1.FileName;
                    pictureBox1.Image = Image.FromFile(fileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);        
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
                    var fileName = saveFileDialog1.FileName;
                    pictureBox1.Image.Save(fileName, ImageFormat.Png);
                    MessageBox.Show("Файл сохранен");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }            
        }

        private void histogramToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {            
            DialogResult result = colorDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
                CurrentColor = colorDialog1.Color;
        }

        private void fileToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pencilButton_Click(object sender, EventArgs e)
        {
            Subscribe("pencil");
            MouseEventHandler handler = line_MouseMove;
            Delegate[] allDelegates = handler.GetInvocationList();
            MessageBox.Show(allDelegates.ToString());
        }

        private void pencil_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                PrevPoint = CurrentPoint;
                CurrentPoint = e.Location;
                Pen p = new Pen(CurrentColor);
                g.DrawLine(p, PrevPoint, CurrentPoint);
            }
        }

        private void pencil_MouseDown(object sender, MouseEventArgs e)
        {
            isPressed = true;
            CurrentPoint = e.Location;
        }

        private void pencil_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }

        private void circle_MouseUp(object sender, MouseEventArgs e)
        {
            //Graphics gr = pictureBox1.CreateGraphics();
            //gr.DrawEllipse(new Pen(Brushes.Red), firstp.X, firstp.Y, e.X - firstp.X, e.Y - firstp.Y);
        }

        private void circle_MouseDown(object sender, MouseEventArgs e)
        {
            //firstp = e.Location;
        }

        private void line_MouseUp(object sender, MouseEventArgs e)
        {   
            if (line != null)
                g.DrawLine(Pens.Red, line.Start, line.End);
        }

        private void line_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                line = new Line { Start = e.Location, End = e.Location };
        }

        private void line_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                line.End = e.Location;                
            }

            //if (line != null)
            //    g.DrawLine(Pens.Red, line.Start, line.End);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //if (line != null)
            //    g.DrawLine(Pens.Red, line.Start, line.End);

            //foreach (var l in lines)
            //    g.DrawLine(Pens.Silver, l.Start, l.End);     
            
        }

        class Line
        {
            public Point Start { get; set; }
            public Point End { get; set; }
        }
        

        private void UnregisteredMouseEvents()
        {
            upEvents = null;
            downEvents = null;
            moveEvents = null;
        }

        private void Subscribe(string sourceElement)
        {
            UnregisteredMouseEvents();
            RegisteredMouseEvents(sourceElement);

            pictureBox1.MouseUp += upEvents;
            pictureBox1.MouseDown += downEvents;
            pictureBox1.MouseMove += moveEvents;
        }

        private void RegisteredMouseEvents(string element)
        {
            switch(element)
            {
                case "pencil":
                    {
                        upEvents += new MouseEventHandler(pencil_MouseUp);
                        downEvents += new MouseEventHandler(pencil_MouseDown);
                        moveEvents += new MouseEventHandler(pencil_MouseMove);
                        break;
                    }
                case "circle":
                    {                        
                        upEvents += new MouseEventHandler(circle_MouseUp);
                        downEvents += new MouseEventHandler(circle_MouseDown);                        
                        break;
                    }
                case "line":
                    {
                        upEvents += new MouseEventHandler(line_MouseUp);
                        downEvents += new MouseEventHandler(line_MouseDown);
                        moveEvents += new MouseEventHandler(line_MouseMove);
                        break;
                    }
            }
        }

        private void circleButton_Click(object sender, EventArgs e)
        {
            Subscribe("circle");
        }

        private void lineButton_Click(object sender, EventArgs e)
        {
            Subscribe("line");
        }
    }
}
